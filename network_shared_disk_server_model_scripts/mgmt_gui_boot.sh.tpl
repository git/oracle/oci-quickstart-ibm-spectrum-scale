#!/bin/bash
## cloud-init bootstrap script

set -x 

touch /tmp/complete
echo "cloud-init setup complete"
set +x

exit 0 
